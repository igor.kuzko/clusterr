import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Card} from '../interfaces/card';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  dataApiUrl: string = 'http://demo2807224.mockable.io/testng';
  constructor(private http: HttpClient) { }

  loadData() {
    return this.http.get(this.dataApiUrl).toPromise().then((res: {endOfFeed: boolean, items: Card[]}): Card[] => {
      console.log('res', res);
      return res.items.filter(item => item.hasOwnProperty('cardType') && item.hasOwnProperty('content'));
    });
  }
}

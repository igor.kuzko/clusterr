/* An entity that represents a card item. */
export interface Card {
  cardType: string;
  content: CardContent;
}

export interface CardContent {
  nrViews: number;
  desc: string;
  source: string;
  docid: string;
  sourceImage: string;
  nrDownVotes: number;
  title: string;
  nrUpVotes: number;
  hasImage: boolean;
  image: CardContentImage;
}

export interface CardContentImage {
  url: string;
  width: number;
  height: number;
}

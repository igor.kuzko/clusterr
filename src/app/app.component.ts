import {Component, HostListener, OnInit} from '@angular/core';
import {DataService} from './services/data.service';
import {Card} from './interfaces/card';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  cardItems: Card[] = [];

  @HostListener('window:resize') onResize(): void {
    const gridWidth = window.innerWidth;
    if (gridWidth < 714) {
      (() => this.resizeAllGridItems(1))();
    } else if (gridWidth < 1056) {
      (() => this.resizeAllGridItems(2))();
    } else {
      (() => this.resizeAllGridItems())();
    }
  }

  constructor(private service: DataService) { }

  ngOnInit() {
    this.service.loadData().then((res: Card[]) => {
      this.cardItems = res;
    });
  }

  resizeAllGridItems(cols = 3, pad = 10) {
    const allItems: any = document.getElementsByClassName('item');
    const gridItem: any = document.getElementById('grid');
    const itemWidth = allItems[0].offsetWidth,
      rowsDimention = {fRow: {top: 0, left: 0}, sRow: {top: 0, left: itemWidth + pad}, tRow: {top: 0, left: 2 * (itemWidth + pad)}};
    let currentRow, gridHeight = 0;

    gridItem.style.width = (cols * (itemWidth + pad) + 30) + 'px';

    for (let i = 0; i < allItems.length; i++) {
      currentRow = 'fRow';
      if (cols > 1 && rowsDimention.sRow.top < rowsDimention[currentRow].top) {
        currentRow = 'sRow';
      }
      if (cols > 2 && rowsDimention.tRow.top < rowsDimention[currentRow].top) {
        currentRow = 'tRow';
      }

      allItems[i].style.top = rowsDimention[currentRow].top + 'px';
      allItems[i].style.left = rowsDimention[currentRow].left + 'px';

      rowsDimention[currentRow].top += (allItems[i].offsetHeight + pad);
      gridHeight = rowsDimention[currentRow].top;
      gridItem.style.height = gridHeight + 'px';
    }
  }

}
